﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="_63120224_Seminar2.Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div style="height: 1275px; width: 807px">
    
        <asp:Label ID="Label1" runat="server" Font-Size="XX-Large" Text="HRM"></asp:Label>
        <br />
        <hr />
        <br />
    
        <asp:Label ID="L_Organizacijska_enota" runat="server" Text="Organizacijska enota:   " Font-Bold="True" Font-Size="Medium"></asp:Label>
        &nbsp;&nbsp;
        <asp:DropDownList ID="DDL_DepartmentGroup" runat="server" AutoPostBack="True" OnSelectedIndexChanged="DDL_DepartmentGroup_SelectedIndexChanged">
        </asp:DropDownList>
        <br />
        <br />
                <asp:Label ID="L_oddelek" runat="server" Text="Oddelek:   " Font-Bold="True" Font-Size="Medium"></asp:Label>
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:DropDownList ID="DDL_Department" runat="server" OnSelectedIndexChanged="DDL_Department_SelectedIndexChanged" AutoPostBack="True">
                </asp:DropDownList>
                <br />
        <hr />
        <br />
        <br />
                <asp:GridView ID="GV_Employee" DataKeyNames="EmployeeID" runat="server" AllowPaging="True" OnSelectedIndexChanged="GV_Employee_SelectedIndexChanged" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanged="GV_Employee_PageIndexChanged" PageSize="5">
                    <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
                    <Columns>
                        <asp:TemplateField HeaderText="Ime in priimek">
                            <ItemTemplate>
                                <asp:Label ID="Label2" runat="server" Text='<%# Eval("Ime_in_priimek") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Delovno mesto">
                            <ItemTemplate>
                                <asp:Label ID="Label3" runat="server" Text='<%# Eval("Delovno_mesto") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Datum zaposlitve">
                            <ItemTemplate>
                                <asp:Label ID="Label4" runat="server" Text='<%# Eval("Datum_zaposlitve", "{0:d}") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:CommandField SelectText="Podrobnosti" ShowSelectButton="True" />
                    </Columns>
                    <EditRowStyle BackColor="#999999" />
                    <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
                    <PagerSettings Mode="NextPrevious" />
                    <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
                    <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
                    <SelectedRowStyle BackColor="Silver" BorderColor="Black" Font-Bold="True" ForeColor="#333333" />
                    <SortedAscendingCellStyle BackColor="#E9E7E2" />
                    <SortedAscendingHeaderStyle BackColor="#506C8C" />
                    <SortedDescendingCellStyle BackColor="#FFFDF8" />
                    <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
                </asp:GridView>
                <br />
        <br />
        <asp:DetailsView ID="DV_EmployeeDetails" runat="server" Height="50px" Width="125px" AutoGenerateRows="False" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <CommandRowStyle BackColor="#E2DED6" Font-Bold="True" />
            <EditRowStyle BackColor="#999999" />
            <FieldHeaderStyle BackColor="#E9ECF1" Font-Bold="True" />
            <Fields>
                <asp:TemplateField HeaderText="Ime in priimek:">
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%# Eval("Ime_in_priimek") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Spol:">
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%# Eval("Spol") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Datum rojstva:">
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text='<%# Eval("Datum_rojstva",  "{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Naslov:">
                    <ItemTemplate>
                        <asp:Label ID="Label8" runat="server" Text='<%# Eval("Naslov") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Telefonska številka:">
                    <ItemTemplate>
                        <asp:Label ID="Label9" runat="server" Text='<%# Eval("Telefonska_stevilka") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Elektronski naslov:">
                    <ItemTemplate>
                        <asp:Label ID="Label10" runat="server" Text='<%# Eval("Elektronski_naslov") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Datum zaposlitve:">
                    <ItemTemplate>
                        <asp:Label ID="Label11" runat="server" Text='<%# Eval("Datum_zaposlitve", "{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Delovna doba:">
                    <ItemTemplate>
                        <asp:Label ID="Label12" runat="server" Text='<%# Eval("Delovna_doba") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Izmena:">
                    <ItemTemplate>
                        <asp:Label ID="Label13" runat="server" Text='<%# Eval("Izmena") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Urna postavka:">
                    <ItemTemplate>
                        <asp:Label ID="Label14" runat="server" Text='<%# Eval("Urna_postavka", "{0:c}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Fields>
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        </asp:DetailsView>
        <hr />
        <br />
        <asp:Button ID="B_Orders" runat="server" Text="Prikaži Ustvarjeni Promet" OnClick="B_Orders_Click" Visible="False" />
        <br />
        <br />
        <asp:Label ID="L_SaleStatistics1" runat="server" Text="Label" Visible="False" BorderColor="#3366FF" BorderStyle="None" BorderWidth="2px" BackColor="#CCFFFF"></asp:Label>
        <br />
        <asp:Label ID="L_SaleStatistics2" runat="server" Text="Label" Visible="False" BorderColor="#99CCFF" BorderStyle="None" BorderWidth="2px" BackColor="#CCFFFF"></asp:Label>
        <br />
        <asp:Label ID="L_SaleStatistics3" runat="server" Text="Label" Visible="False" BorderColor="#3366FF" BorderStyle="None" BorderWidth="2px" BackColor="#CCFFFF"></asp:Label>
        <br />
        <br />
        <asp:GridView ID="GV_Orders" runat="server" Visible="False" AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" GridLines="None">
            <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
            <Columns>
                <asp:TemplateField HeaderText="Naročilo">
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("Narocilo") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Datum">
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("Datum", "{0:d}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Stranka">
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("Stranka") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Število izdelkov">
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("Stevilo_izdelkov") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Vrednost">
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("Vrednost", "{0:c}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Davek">
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("Davek", "{0:c}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Dostava">
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("Dostava", "{0:c}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Za plačilo">
                    <ItemTemplate>
                        <asp:Label ID="Label15" runat="server" Text='<%# Eval("Za_placilo", "{0:c}") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
            <EditRowStyle BackColor="#999999" />
            <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
            <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
            <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
            <SortedAscendingCellStyle BackColor="#E9E7E2" />
            <SortedAscendingHeaderStyle BackColor="#506C8C" />
            <SortedDescendingCellStyle BackColor="#FFFDF8" />
            <SortedDescendingHeaderStyle BackColor="#6F8DAE" />
        </asp:GridView>
    
    </div>
    </form>
</body>
</html>
