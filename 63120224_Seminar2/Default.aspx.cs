﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace _63120224_Seminar2
{
    public partial class Default : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {   
            if (GV_Employee.SelectedIndex < 0 || int.Parse(DDL_Department.SelectedValue) != SalesID())
            {
                B_Orders.Visible = false;
                L_SaleStatistics1.Visible = false;
                L_SaleStatistics2.Visible = false;
                L_SaleStatistics3.Visible = false;
                GV_Orders.Visible = false;
            }
            var sr = new ServiceReference1.HRMServiceClient();
            if (!IsPostBack)
            {
                var query = (from vnos in sr.GetDepartmentList()
                             select new
                             {
                                 vnos.DepartmentGroupName
                             }
                             ).Distinct();
                var luka = query.ToList();
                DDL_DepartmentGroup.DataSource = query.ToList();
                DDL_DepartmentGroup.DataValueField = "DepartmentGroupName";
                DDL_DepartmentGroup.DataBind();
            }
        }

        protected void DDL_Department_SelectedIndexChanged(object sender, EventArgs e)
        {
            var sr = new ServiceReference1.HRMServiceClient();
            var queryGV = (from vnos in sr.GetEmployeeListForDepartment(int.Parse(DDL_Department.SelectedValue))
                           select new
                           {
                               Ime_in_priimek = vnos.FirstName + " " + vnos.LastName,
                               Delovno_mesto = vnos.JobTitle,
                               Datum_zaposlitve = vnos.HireDate,
                               vnos.EmployeeID
                           });
            GV_Employee.DataSource = queryGV.ToList();
            GV_Employee.DataBind();
            GV_Employee.SelectedIndex = -1;
            DV_EmployeeDetails.DataSource = null;
            DV_EmployeeDetails.DataBind();
        }

        protected void GV_Employee_SelectedIndexChanged(object sender, EventArgs e)
        {
            var sr = new ServiceReference1.HRMServiceClient();
            if (GV_Employee.SelectedIndex > -1) {
                var queryDV = (from vnos in sr.GetEmployeeList()
                              where vnos.EmployeeID == int.Parse(GV_Employee.SelectedValue.ToString())
                              select new { 
                              Ime_in_priimek = vnos.FirstName+" "+vnos.LastName,
                              Spol = vnos.Gender,
                              Datum_rojstva = vnos.BirthDate,
                              Naslov = vnos.Address + Environment.NewLine + vnos.City + vnos.PostalCode + Environment.NewLine + vnos.State + Environment.NewLine + vnos.Country,
                              Telefonska_stevilka = vnos.PhoneNumber,
                              Elektronski_naslov = vnos.EmailAddress,
                              Datum_zaposlitve = vnos.HireDate,
                              Delovna_doba = IzracunajDobo(vnos.HireDate)+" let",
                              Delovno_mesto = vnos.JobTitle,
                              Izmena = vnos.Shift,//"Day ("+vnos.ShiftStartTime+" - "+vnos.ShiftEndTime+")",
                              Urna_postavka = vnos.PayRate
                              });
                DV_EmployeeDetails.DataSource = queryDV.ToList();
                DV_EmployeeDetails.DataBind();
                if (int.Parse(DDL_Department.SelectedValue) == SalesID())
               {
                    B_Orders.Visible = true;
               }
            }
        }

        protected void DDL_DepartmentGroup_SelectedIndexChanged(object sender, EventArgs e)
        {
            var sr = new ServiceReference1.HRMServiceClient();
            var queryDDL = (from vnos in sr.GetDepartmentList()
                            where vnos.DepartmentGroupName == DDL_DepartmentGroup.SelectedValue
                            select new
                            {
                                vnos.DepartmentName,
                                vnos.DepartmentID
                            });
            DDL_Department.DataSource = queryDDL.ToList();
            DDL_Department.DataTextField = "DepartmentName";
            DDL_Department.DataValueField = "DepartmentID";
            DDL_Department.DataBind();
            GV_Employee.DataSource = null;
            GV_Employee.DataBind();
            DV_EmployeeDetails.DataSource = null;
            DV_EmployeeDetails.DataBind();
            DDL_Department_SelectedIndexChanged(sender, e);
        }

        protected void B_Orders_Click(object sender, EventArgs e)
        {
            var sr = new ServiceReference1.HRMServiceClient();
            var query1 = (from narocilo in sr.GetOrderList(int.Parse(GV_Employee.SelectedValue.ToString()))
                          select narocilo.Customer).Distinct();
            int stStrank = query1.ToList().Count();
            L_SaleStatistics1.Visible=true;
            L_SaleStatistics2.Visible = true;
            L_SaleStatistics3.Visible = true;
            GV_Orders.Visible = true;
            L_SaleStatistics1.Text = "Število strank: " + stStrank;

            var query2 = (from narocilo in sr.GetOrderList(int.Parse(GV_Employee.SelectedValue.ToString()))
                          select narocilo.OrderNumber).Distinct();
            L_SaleStatistics2.Text = "Število Naročil: " + query2.Count();

            var query3 = (from narocilo in sr.GetOrderList(int.Parse(GV_Employee.SelectedValue.ToString()))
                          select narocilo.SubTotal);
            L_SaleStatistics3.Text = "Skupna vrednost naročil: " + query3.Sum();

            var queryGV = (from narocilo in sr.GetOrderList(int.Parse(GV_Employee.SelectedValue.ToString()))
                           select new { 
                           Narocilo= narocilo.OrderNumber,
                           Datum = narocilo.OrderDate,
                           Stranka = narocilo.Customer,
                           Stevilo_izdelkov=narocilo.Amount,
                           Vrednost = narocilo.SubTotal,
                           Davek= narocilo.TaxAmt,
                           Dostava = narocilo.Freight,
                           Za_placilo= narocilo.TotalDue
                           });
            GV_Orders.DataSource = queryGV.ToList();
            GV_Orders.DataBind();
        }
        protected int SalesID()
        {
            var sr = new ServiceReference1.HRMServiceClient();
            var querrySales = (from L_oddelek in sr.GetDepartmentList()
                               where L_oddelek.DepartmentName.Equals("Sales")
                               select L_oddelek.DepartmentID).First();
            return querrySales;
        }
        protected int IzracunajDobo(DateTime hire)
        {
            DateTime danes = DateTime.Today;
            return danes.Year-hire.Year;
        }

        protected void GV_Employee_PageIndexChanged(object sender, EventArgs e)
        {
        }

        public static Coordinate GetCoordinates(string region)
        {
            using (var client = new System.Net.WebClient())
            {

                string uri = "http://maps.google.com/maps/geo?q='" + region +
                  "'&output=csv&key=ABQIAAAAzr2EBOXUKnm_jVnk0OJI7xSosDVG8KKPE1" +
                  "-m51RBrvYughuyMxQ-i1QfUnH94QxWIa6N4U6MouMmBA";

                string[] geocodeInfo = client.DownloadString(uri).Split(',');

                return new Coordinate(Convert.ToDouble(geocodeInfo[2]),
                           Convert.ToDouble(geocodeInfo[3]));
            }
        }

        public struct Coordinate
        {
            private double lat;
            private double lng;

            public Coordinate(double latitude, double longitude)
            {
                lat = latitude;
                lng = longitude;

            }

            public double Latitude { get { return lat; } set { lat = value; } }
            public double Longitude { get { return lng; } set { lng = value; } }

        }
    }
                
}
